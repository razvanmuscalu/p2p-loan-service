package com.zopa.loan

import scala.util.Try

object MainArgsHelper {

  def checkArgs(args: Array[String]): Option[(String, Int, Int)] =
    args.length match {
      case x if x < 3 =>
        println("You have to provide at least a filename, a loan amount and a loan duration to get a quote")
        None
      case _ =>
        val filename = args(0)
        val amount = Try(args(1).toInt).toOption
        val durationMonths = Try(args(2).toInt).toOption

        (amount, durationMonths) match {
          case (Some(a), Some(d)) => returnArgs(filename, a, d)
          case _ =>
            println("You have to specify amount and duration as integers")
            None
        }
    }

  private def returnArgs(filename: String, a: Int, d: Int) =
    a match {
      case x if x % 100 == 0 && x >= 1000 && x <= 15000 =>
        println(s"File: $filename")
        println(s"Amount: $a")
        println(s"Duration in months: $d")

        Option(filename, a, d)
      case _ =>
        println("Amount has to be in increments of 100 between 1000 and 15000")
        None
    }
}

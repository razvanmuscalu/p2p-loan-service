package com.zopa.loan

import com.zopa.loan.model.LoanRequest
import com.zopa.loan.service.{CsvReader, CsvReaderImpl, LoanService, LoanServiceImpl}

object Main extends App {

  private val csvReader: CsvReader = new CsvReaderImpl()
  private val loanService: LoanService = new LoanServiceImpl(csvReader)

  MainArgsHelper.checkArgs(args).collect {
    case (filename, amount, durationMonths) =>
      val loanResp = loanService.get(LoanRequest(amount, durationMonths), filename)

      loanResp match {
        case Some(loan) =>
          println(s"Total repayment: ${loan.totalRepayment}")
          println(s"Monthly repayment: ${loan.monthlyRepayment}")
          println(s"Offered amount: ${loan.offeredAmount}")
          println(s"Rate: ${loan.rate}")
        case _ =>
          println("We cannot offer you a loan at this time")
      }
  }
}

package com.zopa.loan.service

import com.zopa.loan.model.LenderOffer

import scala.util.Try

trait CsvReader {
  def read(path: String): List[LenderOffer]
}

class CsvReaderImpl() extends CsvReader {
  override def read(path: String): List[LenderOffer] = {
    def read(path: String): List[LenderOffer] =
      for {
        line <- io.Source.fromFile(path).getLines().drop(1).toList
        values = line.split(",").map(_.trim)
      } yield LenderOffer(values(0), values(1).toDouble, values(2).toInt)

    Try(read(path)).getOrElse(List.empty)
  }
}

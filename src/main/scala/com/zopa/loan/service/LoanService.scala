package com.zopa.loan.service

import com.zopa.loan.model.{LoanRequest, LoanResponse}

trait LoanService {
  def get(lr: LoanRequest, path: String): Option[LoanResponse]
}

class LoanServiceImpl(csvReader: CsvReader) extends LoanService {

  def get(loanReg: LoanRequest, path: String): Option[LoanResponse] = {
    def to2decimals(x: Double): Double = math.floor(x * 100) / 100
    def to1decimal(x: Double): Double = math.floor(x * 10) / 10
    def compound(amount: Int, rate: Double, monthsDuration: Int): Double = amount * math.pow(1 + rate / 100, monthsDuration.toDouble / 12)

    csvReader.read(path) match {
      case lenderOffers if lenderOffers.nonEmpty =>
        val bestLenderOffers = LoanHelper.collectLenderOffers(loanReg.amount, lenderOffers)

        val rawTotalRepayment = bestLenderOffers.map(o => (o.amount * o.rate) + o.amount).sum
        val offeredAmount = bestLenderOffers.map(_.amount).sum
        val loanRate = to1decimal(((rawTotalRepayment * 100) / offeredAmount) - 100)
        val compoundedTotalRepayment = to2decimals(compound(offeredAmount, loanRate, loanReg.monthsDuration))
        val monthlyRepayment = to2decimals(compoundedTotalRepayment / loanReg.monthsDuration)

        Option(LoanResponse(offeredAmount, loanRate, monthlyRepayment, compoundedTotalRepayment))
      case _ => None
    }
  }
}

package com.zopa.loan.service

import com.zopa.loan.model.LenderOffer

import scala.annotation.tailrec

object LoanHelper {
  def collectLenderOffers(amount: Int, in: List[LenderOffer]): List[LenderOffer] =
    amount match {
      case x if x > 0 => collectLenderOffers(amount, in, 0, List.empty)
      case _          => List.empty
    }

  @tailrec
  private def collectLenderOffers(amount: Int,
                                  lenderOffers: List[LenderOffer],
                                  accumulatedAmount: Int,
                                  accumulatedLenderOffers: List[LenderOffer]): List[LenderOffer] =
    if (accumulatedAmount == amount || lenderOffers.isEmpty) {
      accumulatedLenderOffers
    } else {
      val bestNextLenderOffer = lenderOffers.minBy(_.rate)
      val reducedLenderOffers = lenderOffers.filterNot(_.lender == bestNextLenderOffer.lender)

      amount - accumulatedAmount - bestNextLenderOffer.amount match {
        case x if x > 0 =>
          val increasedAmount = accumulatedAmount + bestNextLenderOffer.amount
          val increasedLenderOffers = bestNextLenderOffer :: accumulatedLenderOffers
          collectLenderOffers(amount, reducedLenderOffers, increasedAmount, increasedLenderOffers)
        case _ =>
          val increasedAmount = accumulatedAmount + (amount - accumulatedAmount)
          val increasedLenderOffers = bestNextLenderOffer.copy(amount = amount - accumulatedAmount) :: accumulatedLenderOffers
          collectLenderOffers(amount, reducedLenderOffers, increasedAmount, increasedLenderOffers)
      }
    }
}

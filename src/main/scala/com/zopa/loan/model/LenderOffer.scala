package com.zopa.loan.model

case class LenderOffer(lender: String, rate: Double, amount: Int)

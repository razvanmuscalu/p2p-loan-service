package com.zopa.loan.model

case class LoanResponse(offeredAmount: Int, rate: Double, monthlyRepayment: Double, totalRepayment: Double)

package com.zopa.loan.model

case class LoanRequest(amount: Int, monthsDuration: Int)

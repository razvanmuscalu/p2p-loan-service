package com.zopa.loan

import org.scalatest.prop.{GeneratorDrivenPropertyChecks, TableDrivenPropertyChecks}

class MainArgsHelperTest extends BaseTest with GeneratorDrivenPropertyChecks with TableDrivenPropertyChecks {

  private val argsExamples = Table(
    (
      "testCase",
      "args",
      "out"
    ),
    (
      "return None when 1 arg provided",
      Array(""),
      None
    ),
    (
      "return None when 2 args provided",
      Array("", ""),
      None
    ),
    (
      "return None when 3 args provided but 2nd arg not integer",
      Array("", "", "24"),
      None
    ),
    (
      "return None when 3 args provided but 3nd arg not integer",
      Array("", "100", ""),
      None
    ),
    (
      "return checked args",
      Array("testFilePath", "1100", "24"),
      Some("testFilePath", 1100, 24)
    ),
    (
      "return checked args when more than 3 args",
      Array("testFilePath", "1100", "24", "unknown"),
      Some("testFilePath", 1100, 24)
    ),
    (
      "return None when amount is not increment of 100",
      Array("testFilePath", "1185", "24"),
      None
    ),
    (
      "return None when amount is lower than 1000",
      Array("testFilePath", "900", "24"),
      None
    ),
    (
      "return None when amount is higher than 15000",
      Array("testFilePath", "15500", "24"),
      None
    )
  )

  "MainArgsHelper" should {
    "return checked args" in {
      forAll(argsExamples) { (_, args, out) =>
        MainArgsHelper.checkArgs(args) shouldBe out
      }
    }
  }
}

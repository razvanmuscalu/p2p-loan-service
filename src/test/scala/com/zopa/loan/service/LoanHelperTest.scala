package com.zopa.loan.service

import com.zopa.loan.BaseTest
import com.zopa.loan.model.LenderOffer
import org.scalatest.prop.{GeneratorDrivenPropertyChecks, TableDrivenPropertyChecks}

class LoanHelperTest extends BaseTest with GeneratorDrivenPropertyChecks with TableDrivenPropertyChecks {

  private val lenderOffersExamples = Table(
    (
      "testCase",
      "amount",
      "in",
      "out"
    ),
    (
      "return offer when a single lender offer",
      100,
      List(
        LenderOffer("Fred", 0.075, 100)
      ),
      List(
        LenderOffer("Fred", 0.075, 100)
      )
    ),
    (
      "return offer when a single lender offer with more than the amount requested",
      50,
      List(
        LenderOffer("Fred", 0.075, 100)
      ),
      List(
        LenderOffer("Fred", 0.075, 50)
      )
    ),
    (
      "return best offers when requested amount matches exactly the sum of the found offers",
      300,
      List(
        LenderOffer("Maria", 0.065, 100),
        LenderOffer("Fred", 0.075, 100),
        LenderOffer("Mary", 0.085, 100),
        LenderOffer("Bob", 0.095, 100)
      ),
      List(
        LenderOffer("Mary", 0.085, 100),
        LenderOffer("Fred", 0.075, 100),
        LenderOffer("Maria", 0.065, 100)
      )
    ),
    (
      "handle random order of lender offers with respect to the rate when when requested amount matches exactly the sum of the found offers",
      300,
      List(
        LenderOffer("Mary", 0.085, 100),
        LenderOffer("Maria", 0.065, 100),
        LenderOffer("Bob", 0.095, 100),
        LenderOffer("Fred", 0.075, 100)
      ),
      List(
        LenderOffer("Mary", 0.085, 100),
        LenderOffer("Fred", 0.075, 100),
        LenderOffer("Maria", 0.065, 100)
      )
    ),
    (
      "return best offers when worse offer to cover up the requested amount has more than the amount still left to cover",
      250,
      List(
        LenderOffer("Maria", 0.065, 100),
        LenderOffer("Fred", 0.075, 100),
        LenderOffer("Mary", 0.085, 100),
        LenderOffer("Bob", 0.095, 100)
      ),
      List(
        LenderOffer("Mary", 0.085, 50),
        LenderOffer("Fred", 0.075, 100),
        LenderOffer("Maria", 0.065, 100)
      )
    ),
    (
      "handle random order of offers with respect to the rate when worse offer to cover up the requested amount has more than the amount still left to cover",
      250,
      List(
        LenderOffer("Bob", 0.095, 100),
        LenderOffer("Maria", 0.065, 100),
        LenderOffer("Mary", 0.085, 100),
        LenderOffer("Fred", 0.075, 100)
      ),
      List(
        LenderOffer("Mary", 0.085, 50),
        LenderOffer("Fred", 0.075, 100),
        LenderOffer("Maria", 0.065, 100)
      )
    ),
    (
      "return offer with amount when there is a single offer that is less than the requested amount",
      100,
      List(
        LenderOffer("Fred", 0.075, 50)
      ),
      List(
        LenderOffer("Fred", 0.075, 50)
      )
    ),
    (
      "return best offers when worse offer to cover up the requested amount has less than the amount still left to cover",
      450,
      List(
        LenderOffer("Maria", 0.065, 100),
        LenderOffer("Fred", 0.075, 100),
        LenderOffer("Mary", 0.085, 100),
        LenderOffer("Bob", 0.095, 100)
      ),
      List(
        LenderOffer("Bob", 0.095, 100),
        LenderOffer("Mary", 0.085, 100),
        LenderOffer("Fred", 0.075, 100),
        LenderOffer("Maria", 0.065, 100)
      )
    ),
    (
      "handle when there are no lender offers",
      100,
      List.empty,
      List.empty
    ),
    (
      "handle when requested amount is negative",
      -100,
      List(
        LenderOffer("Fred", 0.075, 50)
      ),
      List.empty
    )
  )

  "LoanHelper" should {
    "return best offers for user" in {
      forAll(lenderOffersExamples) { (_, amount, in, out) =>
        LoanHelper.collectLenderOffers(amount, in) shouldBe out
      }
    }
  }
}

package com.zopa.loan.service

import java.io.File

import com.zopa.loan.BaseTest
import com.zopa.loan.model.LenderOffer

class CsvReaderTest extends BaseTest {

  private val csvReader = new CsvReaderImpl()

  "CsvReader" should {
    "read csv into list of lender offers" in {
      val exp = List(
        LenderOffer("Bob", 0.075, 640),
        LenderOffer("Jane", 0.069, 480),
        LenderOffer("Fred", 0.071, 520),
        LenderOffer("Mary", 0.104, 170),
        LenderOffer("John", 0.081, 320),
        LenderOffer("Dave", 0.074, 140),
        LenderOffer("Angela", 0.071, 60)
      )

      val testFile = new File(getClass.getClassLoader.getResource("mocks/test-lender-offers.csv").getPath).toString
      val out = csvReader.read(testFile)

      out shouldBe exp
    }

    "return empty list of lender offers when file not found" in {
      val out = csvReader.read("unknown")

      out shouldBe List.empty
    }
  }
}

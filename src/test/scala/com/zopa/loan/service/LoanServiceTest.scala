package com.zopa.loan.service

import com.zopa.loan.BaseTest
import com.zopa.loan.model.{LenderOffer, LoanRequest, LoanResponse}
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.BeforeAndAfterEach
import org.scalatest.mockito.MockitoSugar
import org.scalatest.prop.{GeneratorDrivenPropertyChecks, TableDrivenPropertyChecks}

class LoanServiceTest extends BaseTest with MockitoSugar with BeforeAndAfterEach with GeneratorDrivenPropertyChecks with TableDrivenPropertyChecks {

  private val path = "somePath"
  private val csvReader = mock[CsvReader]
  private val loanService = new LoanServiceImpl(csvReader)

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    reset(csvReader)
  }

  "LoanService" should {
    "return loan when there is a single lender offer which matches requested amount" in {
      when(csvReader.read(any[String])).thenReturn(List(LenderOffer("Fred", 0.075, 10000)))

      val out = loanService.get(LoanRequest(10000, 6), path)

      out shouldBe Some(LoanResponse(10000, 7.5, 1728.03, 10368.22))
    }

    "return loan when there is a single lender offer which is less than requested amount" in {
      when(csvReader.read(any[String])).thenReturn(List(LenderOffer("Fred", 0.075, 5000)))

      val out = loanService.get(LoanRequest(10000, 6), path)

      out shouldBe Some(LoanResponse(5000, 7.5, 864.01, 5184.11))
    }

    "return None when there are no lender offers" in {
      when(csvReader.read(any[String])).thenReturn(List.empty)

      val out = loanService.get(LoanRequest(10000, 6), path)

      out shouldBe None
    }

    "return correct loan amount when there are multiple offers" in {
      val offers = List(
        LenderOffer("Fred", 0.055, 10000),
        LenderOffer("Bob", 0.125, 5000)
      )
      when(csvReader.read(any[String])).thenReturn(offers)

      val out = loanService.get(LoanRequest(30000, 6), path)

      out.get.offeredAmount shouldBe 15000
    }

    "return correct loan rate" in {
      val lenderOffersExamples = Table(
        (
          "testCase",
          "in",
          "out"
        ),
        (
          "1 lender offer",
          List(
            LenderOffer("Fred", 0.05, 1000)
          ),
          5
        ),
        (
          "2 lender offers",
          List(
            LenderOffer("Fred", 0.03, 1000),
            LenderOffer("Bob", 0.09, 1000)
          ),
          6
        ),
        (
          "2 lender offers with different amounts",
          List(
            LenderOffer("Fred", 0.03, 1000),
            LenderOffer("Bob", 0.09, 2000)
          ),
          7
        )
      )

      forAll(lenderOffersExamples) { (_, in, out) =>
        when(csvReader.read(any[String])).thenReturn(in)
        loanService.get(LoanRequest(3000, 36), path).get.rate shouldBe out
      }
    }

    "return correct correct compounded total repayment" in {
      val lenderOffersExamples = Table(
        (
          "testCase",
          "in",
          "durationMonths",
          "out"
        ),
        (
          "5% over 6 months",
          List(LenderOffer("Fred", 0.05, 1000)),
          6,
          1024.69
        ),
        (
          "5% over 12 months",
          List(LenderOffer("Fred", 0.05, 1000)),
          12,
          1050
        ),
        (
          "5% over 18 months",
          List(LenderOffer("Fred", 0.05, 1000)),
          18,
          1075.92
        ),
        (
          "5% over 24 months",
          List(LenderOffer("Fred", 0.05, 1000)),
          24,
          1102.5
        ),
        (
          "5% over 36 months",
          List(LenderOffer("Fred", 0.05, 1000)),
          36,
          1157.62
        ),
        (
          "5% over 48 months",
          List(LenderOffer("Fred", 0.05, 1000)),
          48,
          1215.5
        )
      )

      forAll(lenderOffersExamples) { (_, in, durationMonths, out) =>
        when(csvReader.read(any[String])).thenReturn(in)
        loanService.get(LoanRequest(3000, durationMonths), path).get.totalRepayment shouldBe out
      }
    }

    "return correct loan monthly repayment" in {
      val lenderOffersExamples = Table(
        (
          "testCase",
          "in",
          "durationMonths",
          "out"
        ),
        (
          "5% over 6 months",
          List(LenderOffer("Fred", 0.05, 1000)),
          6,
          170.78
        ),
        (
          "5% over 12 months",
          List(LenderOffer("Fred", 0.05, 1000)),
          12,
          87.5
        ),
        (
          "5% over 18 months",
          List(LenderOffer("Fred", 0.05, 1000)),
          18,
          59.77
        ),
        (
          "5% over 24 months",
          List(LenderOffer("Fred", 0.05, 1000)),
          24,
          45.93
        ),
        (
          "5% over 36 months",
          List(LenderOffer("Fred", 0.05, 1000)),
          36,
          32.15
        ),
        (
          "5% over 48 months",
          List(LenderOffer("Fred", 0.05, 1000)),
          48,
          25.32
        )
      )

      forAll(lenderOffersExamples) { (_, in, durationMonths, out) =>
        when(csvReader.read(any[String])).thenReturn(in)
        loanService.get(LoanRequest(3000, durationMonths), path).get.monthlyRepayment shouldBe out
      }
    }
  }
}

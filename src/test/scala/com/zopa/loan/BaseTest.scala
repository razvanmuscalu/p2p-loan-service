package com.zopa.loan

import org.scalatest._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.prop.Checkers
import org.scalatest.time.{Millis, Span}

import scala.io.Source

trait BaseTest extends WordSpec with Matchers with ScalaFutures with Checkers {
  implicit val pc: PatienceConfig = PatienceConfig(timeout = scaled(Span(15000, Millis)), interval = scaled(Span(100, Millis)))

  def read(path: String): String = Source.fromInputStream(getClass.getResourceAsStream(path)).mkString
}

# p2p-loan-service

# package and run

You can run `mvn clean package` to run tests and create a runnable JAR which will be created under `target/docker-src` folder.

You can then run `java -jar <JAR file> arg1 arg2 arg3` where

- `arg1` is the csv file
- `arg2` is the loan amount
- `arg3` is the loan duration in months

I included a JAR called `p2p-loan-service.jar` already under the root directory if you want to use that.

# Assumptions & Decisions

I assumed that the offered loan can be accumulated from a range of lender offers. So I am picking the best lender offers with the lowest rate until I fulfil the requested amount and then calculate and return the loan.

I went for a tail recursive function to find these best lender offers. This works well since I am operating on a static csv file. In reality, where there would be concurrent requests operating on the same pool of lender offers, a different strategy might have to be adopted.

In case there are not enough lender offers to cover the requested amount, I am still returning a loan offer but at a lower amount.
I work in the partner integrations team at ClearScore and I saw that this behaviour happens in the industry so I decided to do it this way. It would be a small code change to not return a loan offer at all instead.

I also extended the exercise a bit by asking the user for a loan duration in months, instead of hardcoding to 36 months. Again, this is business as usual behaviour in the industry.

My rate calculation is very simple and common sense (average of lender offers' rates weighted with lender offers' amounts), in reality it might be a more complex formula to calculate it.

I assumed the rates are applied once a year (the compound formula can be easily extended to allow for once a month, once a quarter, etc.).

